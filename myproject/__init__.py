import requests
import datetime
import os
import time
from flask import Flask, request, Response, render_template, flash
from flask.ext.sqlalchemy import SQLAlchemy

SQLALCHEMY_DATABASE_URI = 'sqlite:///db/games.db'

app = Flask(__name__)
SECRET_KEY = 'supersecretkeytoberemovedlater'
app.config.from_object(__name__)
app.debug = True

db = SQLAlchemy(app)

import myproject.views
import myproject.models

def formatDate(dateString):
    """This function takes a date string in the format 2017-01-04T14:00:00.000 as input and returns a python datetime object as output"""
    formattedString = dateString[0:10] + ' ' + dateString[11:19]
    date = datetime.datetime.strptime(formattedString, "%Y-%m-%d %H:%M:%S")
    return(date)

def formatMessage(previous, update):
    """This function is to be used when updating a games object's status or notes attribute.  This function takes a status or notes string and updates it with the current message and time.  It then outputs the new status or note as a string."""
    prevList = eval(previous)
    prevList.append((datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), update))
    currentString = str(prevList)
    return(currentString)

partnersList = ['Brown', 'Columbia', 'Cornell', 'Harvard', 'Penn', 'Princeton', 'Yale', 'Colorado College', 'Denver', 'Miami', 'Minnesota Duluth', "Omaha", "North Dakota", "St. Cloud State", "Western Michigan", "Furman", "UNCG", "Citadel", "ETSU", "Mercer", "Samford", "Chattanooga", "VMI", "WCU", "Wofford", "UCF"]

contactInfo = {
    "Brown": {"name": "Liz Colleran", "phone": "401-225-0854", "email": "elizabeth_colleran@brown.edu"},
    "Columbia": {"name": "Alex Oberweger", "phone": "347-756-0355", "email": "ajo13@columbia.edu"},
    "Cornell": {"name": "John Lukach", "phone": "330-309-4695", "email": "jlukach@cornell.edu"},
    "Harvard": {"name": "Imry Halevi", "phone": "857-891-4012", "email": "ihalevi@fas.harvard.edu"},
    "Penn": {"name": "Ryan Koletty", "phone": "215-450-2737", "email": "rkoletty@upenn.edu"},
    "Princeton": {"name": "Cody Crusciel", "phone": "973-800-2097", "email": "codyc@princeton.edu"},
    "Yale": {"name": "Sam Rubin", "phone": "203-314-2668", "email": "sam.rubin@yale.edu"},
    "Colorado College": {"name": "Jerry Cross", "phone": "719-389-6755", "email": "jerry.cross@coloradocollege.edu"},
    "Denver": {"name": "Rick Bowness", "phone": "303-871-2390", "email": "richard.bowness@du.edu"},
    "Miami": {"name": "Steve Baker", "phone": "513-330-2100", "email": "bakersa@miamioh.edu"},
    "Minnesota Duluth": {"name": "Bob Nygaard", "phone": "218-726-8191", "email": "bnygaard@d.umn.edu"},
    "Omaha": {"name": "Dave Ahlers", "phone": "402-554-3387", "email": "dahlers@unomaha.edu"},
    "North Dakota": {"name": "Kyle Doperalski", "phone": "701-777-2210", "email": "kyle.doperalski@athletics.und.edu"},
    "St. Cloud State": {"name": "Joel Larsen", "phone": "320-308-2293", "email": "jmlarsen@stscloudstate.edu"},
    "Western Michigan": {"name": "Monty Porter", "phone": "269-387-3090", "email": "monty.porter@wmich.edu"},
    "Furman": {"name": "Mike Arnold", "phone": "864-294-3267", "email": "mike.arnold@furman.edu"},
    "UNCG": {"name": "Matt McCollester", "phone": "336-334-5615", "email": "matt.mccollester@uncg.edu"},
    "Citadel": {"name": "Caleb Furrow", "phone": "843-953-9965", "email": "cfurrow@citadel.edu"},
    "ETSU": {"name": "Steven May", "phone": "540-922-2999", "email": "maysm1@etsu.edu"},
    "Mercer": {"name": "Lisa Cherry", "phone": "478-301-2813", "email": "cherry_lj@mercer.edu"},
    "Samford": {"name": "Kayla Shaffer", "phone": "205-726-4148", "email": "kshaffer@samford.edu"},
    "Chattanooga": {"name": "Leah Gill", "phone": "423-779-4033", "email": "leah-gill@utc.edu"},
    "VMI": {"name": "Wade Branner", "phone": "540-464-7515", "email": "brannerwh@vmi.edu"},
    "WCU": {"name": "Will Adams", "phone": "336-601-3578", "email": "rwadams@email.wcu.edu"},
    "Wofford": {"name": "Garrett Hall", "phone": "864-597-4145", "email": "hallgd@wofford.edu"},
    "UCF": {"name": "Bobby Blankenship", "phone": "239-223-8255", "email": "bblankenship@athletics.ucf.edu"}
    }



if __name__ == "__main__":
    application.run(host='0.0.0.0')


