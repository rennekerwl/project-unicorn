// jQuery Docs http://api.jquery.com/
// jQuery Style Guide https://github.com/voorhoede/jquery-style-guide

$(document).ready(function() {
    $('.show-form').on('click', function(event) {
        $(this).closest('.thumbnail').find('.hidden-form').toggleClass('hidden');
    });

    $('#ivy-panel').on('click', function(event) {
        if ($('#league-text').text() == 'IVY') {
            $('#league-text').closest('#event-row').toggle();
        } else {
            console.log($('#league-text').text());
            //
        }
    });

    $(function () {
        $('#datetimepicker1').datetimepicker();
    });
});
