import requests
import datetime
import os
import time
from flask import Flask, request, Response, render_template, flash, redirect, url_for
from flask.ext.sqlalchemy import SQLAlchemy

from myproject import app
from myproject import models
from . import db

def formatDate(dateString):
    """This function takes a date string in the format 2017-01-04T14:00:00.000 as input and returns a python datetime object as output"""
    formattedString = dateString[0:10] + ' ' + dateString[11:19]
    date = datetime.datetime.strptime(formattedString, "%Y-%m-%d %H:%M:%S")
    return(date)

def formatMessage(previous, update):
    """This function is to be used when updating a games object's status or notes attribute.  This function takes a status or notes string and updates it with the current message and time.  It then outputs the new status or note as a string."""
    prevList = eval(previous)
    prevList.append((datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), update))
    currentString = str(prevList)
    return(currentString)

partnersList = ['Brown', 'Columbia', 'Cornell', 'Harvard', 'Penn', 'Princeton', 'Yale', 'Colorado College', 'Denver', 'Miami', 'Minnesota Duluth', "Omaha", "North Dakota", "St. Cloud State", "Western Michigan", "Furman", "UNCG", "Citadel", "ETSU", "Mercer", "Samford", "Chattanooga", "VMI", "WCU", "Wofford", "UCF"]

contactInfo = {
    "Brown": {"name": "Liz Colleran", "phone": "401-225-0854", "email": "elizabeth_colleran@brown.edu"},
    "Columbia": {"name": "Alex Oberweger", "phone": "347-756-0355", "email": "ajo13@columbia.edu"},
    "Cornell": {"name": "John Lukach", "phone": "330-309-4695", "email": "jlukach@cornell.edu"},
    "Harvard": {"name": "Imry Halevi", "phone": "857-891-4012", "email": "ihalevi@fas.harvard.edu"},
    "Penn": {"name": "Justin Diaz", "phone": "484-643-3131", "email": "justdiaz@upenn.edu"},
    "Princeton": {"name": "Cody Crusciel", "phone": "973-800-2097", "email": "codyc@princeton.edu"},
    "Yale": {"name": "Sam Rubin", "phone": "203-314-2668", "email": "sam.rubin@yale.edu"},
    "Colorado College": {"name": "Jerry Cross", "phone": "719-389-6755", "email": "jerry.cross@coloradocollege.edu"},
    "Denver": {"name": "Rick Bowness", "phone": "303-871-2390", "email": "richard.bowness@du.edu"},
    "Miami": {"name": "Steve Baker", "phone": "513-330-2100", "email": "bakersa@miamioh.edu"},
    "Minnesota Duluth": {"name": "Bob Nygaard", "phone": "218-726-8191", "email": "bnygaard@d.umn.edu"},
    "Omaha": {"name": "Dave Ahlers", "phone": "402-554-3387", "email": "dahlers@unomaha.edu"},
    "North Dakota": {"name": "Kyle Doperalski", "phone": "701-777-2210", "email": "kyle.doperalski@athletics.und.edu"},
    "St. Cloud State": {"name": "Joel Larsen", "phone": "320-308-2293", "email": "jmlarsen@stscloudstate.edu"},
    "Western Michigan": {"name": "Monty Porter", "phone": "269-387-3090", "email": "monty.porter@wmich.edu"},
    "Furman": {"name": "Caleb Furrow", "phone": "864-294-2771", "email": "caleb.furrow@furman.edu"},
    "UNCG": {"name": "Tony Hermane", "phone": "336-404-8172", "email": "a_herman@uncg.edu"},
    "Citadel": {"name": "Connor Putman", "phone": "719-352-2676", "email": "cputman@citadel.edu"},
    "ETSU": {"name": "Steven May", "phone": "540-922-2999", "email": "maysm1@etsu.edu"},
    "Mercer": {"name": "Lisa Cherry", "phone": "478-301-2813", "email": "cherry_lj@mercer.edu"},
    "Samford": {"name": "Kayla Shaffer", "phone": "205-726-4148", "email": "kshaffer@samford.edu"},
    "Chattanooga": {"name": "Leah Gill", "phone": "423-779-4033", "email": "leah-gill@utc.edu"},
    "VMI": {"name": "Wade Branner", "phone": "540-464-7515", "email": "brannerwh@vmi.edu"},
    "WCU": {"name": "Will Adams", "phone": "336-601-3578", "email": "rwadams@email.wcu.edu"},
    "Wofford": {"name": "Garrett Hall", "phone": "864-597-4145", "email": "hallgd@wofford.edu"},
    "UCF": {"name": "Bobby Blankenship", "phone": "239-223-8255", "email": "bblankenship@athletics.ucf.edu"}
    }


@app.before_first_request
def init_request():
    db.create_all()

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    today = time.strftime("%m-%d-%Y")
    return redirect(url_for("showDate", date=today))



@app.route('/date/<date>', methods=['GET', 'POST'])
def showDate(date):
    """This view returns the stored events for a past or future date.  Date is a string in the format 03-17-2017-1:49-PM"""
    splitDate = date.split('-')
    dateToSearch = datetime.date(int(splitDate[2][0:4]), int(splitDate[0]), int(splitDate[1]))
    #Get games stored for the day
    allStoredGames = models.games.query.all()
    allEventIDs = [game.eventID for game in allStoredGames]
    daysStoredGames = [game for game in allStoredGames if game.date.date() == dateToSearch]


    #if information submitted
    if request.method == 'POST':
        #Get ID number of submitted event
        #NOT WORKING
        submittedID = request.form['eventID']
        submittedNotes = request.form['notes']
        submittedStatus = request.form['options']
        #Update game's notes and status attributes
        for game in daysStoredGames:
            if str(game.eventID) == submittedID:
                statuses = eval(game.status)
                now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                statuses.append((now, submittedStatus))
                game.status = str(statuses)

                notes = eval(game.notes)
                notes.append((now, submittedNotes))
                game.notes = str(notes)

                db.session.commit()
                # #Update stored games list to show the new information
                # daysStoredGames = [game for game in allStoredGames if game.date.date() == dateToSearch]
                # daysStoredGames.sort(key=lambda x: x.date)


    day = dateToSearch.strftime("%Y-%m-%d")
    daysGames = []
    ivyURL = "http://www.ivyleaguenetwork.com/ivyleague/schedule?format=json" + "&date=" + day
    soconURL = "http://www.socondigitalnetwork.com/socon/schedule?format=json" + "&date=" + day
    ucfURL = "http://www.ucfknights.tv/schedule?format=json" + "&date=" + day
    nchcURL = "http://www.nchc.tv/schedule?format=json" + "&date=" + day
    # Remove Ivy League, no longer a partner
    urls = [soconURL, ucfURL, nchcURL]

    #Get games schedule for day from websites
    for url in urls:
        r = requests.get(url)
        data = r.json()
        for game in data['games']:
            if day in game['date']:
                if url == ivyURL:
                    game['league'] = 'IVY'
                elif url == soconURL:
                    game['league'] = 'SOCON'
                elif url == ucfURL:
                    game['league'] = 'UCF'
                elif url == nchcURL:
                    game['league'] = 'NCHC'
                else:
                    game['league'] = 'LEAGUE'
                #Check game['isGame'] to see if there are home/away team names
                if game['isGame'] == 'true':
                    daysGames.append(game)
                if game['isGame'] == 'false':
                    game['homeTeam'] = {}
                    game['homeTeam']['name'] = game['name']
                    game['awayTeam'] = {}
                    game['awayTeam']['name'] = game['name']
                    daysGames.append(game)

    #If there are games already stored for day then store only new games
    if bool(daysStoredGames):
        for tGame in daysGames:
            if tGame['id'] not in [str(game.eventID) for game in daysStoredGames]:
                #get contact info
                if tGame['homeTeam']['name'] not in partnersList:
                    contactInfoString = "No Contact Info"
                elif tGame['homeTeam']['name'] in partnersList:
                    contactInfoString = contactInfo[tGame['homeTeam']['name']]['name'] + ' ' + contactInfo[tGame['homeTeam']['name']]['phone'] + ' ' + contactInfo[tGame['homeTeam']['name']]['email']
                #Build games object from json data
                gameToStore = models.games(
                    eventID = int(tGame['id']),
                    seoName = tGame['seoName'],
                    date = formatDate(tGame['date']),
                    sport = tGame['sportName'],
                    extID = tGame['extId'],
                    gameState = int(tGame['gameState']),
                    awayTeam = tGame['awayTeam']['name'],
                    homeTeam = tGame['homeTeam']['name'],
                    contact = contactInfoString,
                    status = str([(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), 'NA')]),
                    notes = str([(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), 'No notes.')]),
                    league = tGame['league'])
                #Double check to make sure game hasn't already been added?
                if gameToStore.eventID not in allEventIDs:
                    #Add games object to database
                    db.session.add(gameToStore)
                    db.session.commit()
                #If game is already in a different day then update that game's date instead of making a new game object w/ the same ID
                elif gameToStore.eventID in allEventIDs:
                    for game in allStoredGames:
                        if game.eventID == gameToStore.eventID:
                            game.seoName = gameToStore.seoName
                            game.date = gameToStore.date
                            db.session.commit()

            #Check already stored games for time/date, seoName, external ID, and gameState changes
            elif tGame['id'] in [str(game.eventID) for game in daysStoredGames]:
                for game in daysStoredGames:
                    if str(game.eventID) == tGame['id']:
                        if game.date != formatDate(tGame['date']):
                            game.date = formatDate(tGame['date'])
                            db.session.commit()
                        if game.seoName != tGame['seoName']:
                            game.seoName = tGame['seoName']
                            db.session.commit()
                        if game.extID != tGame['extId']:
                            game.extID = tGame['extId']
                            db.session.commit()
                        if game.gameState != tGame['gameState']:
                            game.gameState = tGame['gameState']
                            db.session.commit()


    #If there aren't any games already stored today then store all games
    else:
        for tGame in daysGames:
            #get contact info
            if tGame['homeTeam']['name'] not in partnersList:
                contactInfoString = "No Contact Info"
            elif tGame['homeTeam']['name'] in partnersList:
                contactInfoString = contactInfo[tGame['homeTeam']['name']]['name'] + ' ' + contactInfo[tGame['homeTeam']['name']]['phone'] + ' ' + contactInfo[tGame['homeTeam']['name']]['email']
            #Build games object from json data
            gameToStore = models.games(
                eventID = int(tGame['id']),
                seoName = tGame['seoName'],
                date = formatDate(tGame['date']),
                sport = tGame['sportName'],
                extID = tGame['extId'],
                awayTeam = tGame['awayTeam']['name'],
                homeTeam = tGame['homeTeam']['name'],
                contact = contactInfoString,
                status = str([(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), 'NA')]),
                notes = str([(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), 'No notes.')]),
                league = tGame['league'])
            #Double check to make sure game hasn't already been added?

            if gameToStore.eventID not in allEventIDs:
                #Add games object to database
                db.session.add(gameToStore)
                db.session.commit()

    # Search for games againd and sort games prior to displaying
    allStoredGames = models.games.query.all()
    daysStoredGames = [game for game in allStoredGames if game.date.date() == dateToSearch]
    daysStoredGames.sort(key=lambda x: x.date)                
    #Show Page
    return render_template('index.html', todayStoredGames=daysStoredGames)
    

@app.route('/contact', methods=['GET'])
def contact():
    return render_template('contact.html')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
