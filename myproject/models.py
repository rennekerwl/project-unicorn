from . import db


class games(db.Model):
    __tablename__ = 'storedgames'
    eventID = db.Column(db.Integer, primary_key = True)
    seoName = db.Column(db.String)
    date = db.Column(db.DateTime)
    sport = db.Column(db.String)
    extID = db.Column(db.String)
    gameState = db.Column(db.Integer)
    awayTeam = db.Column(db.String)
    homeTeam = db.Column(db.String)
    contact = db.Column(db.String)
    status = db.Column(db.String)
    notes = db.Column(db.String)
    league = db.Column(db.String)
